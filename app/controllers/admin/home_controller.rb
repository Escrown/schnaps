class Admin::HomeController < AdminController
  before_action :authenticate_user! if ENV != 'staging'

  def index
  end

  def empty
    OrderItem.delete_all
    Order.delete_all
    Product.delete_all
    redirect_to admin_root_path
  end

  def reporting
    @current_year = Date.today.year
    @current_month = Date.today.month

    @last_month = Date.today.month == 1 ? 12 : (Date.today.month - 1)
    @last_year = @last_month == 12 ? (@current_year - 1.year) : @current_year

    start_last_month = Date.new(@last_year, @last_month, 1).beginning_of_month
    end_last_month = Date.new(@last_year, @last_month, 1).at_end_of_month
    last_month_range =  start_last_month..end_last_month
    last_month_order_items = OrderItem.where(created_at: last_month_range).all
    @last_month_revenue = last_month_order_items.sum { |oi| oi.gross }

    current_start_date = Date.new(@current_year, @current_month, 1).beginning_of_month
    current_end_date = Date.new(@current_year, @current_month, 1).at_end_of_month
    current_month_range = current_start_date..current_end_date
    current_month_order_items = OrderItem.where(created_at: current_month_range).all
    @this_month_revenue = current_month_order_items.sum { |oi| oi.gross }
  end

end