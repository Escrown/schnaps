class Admin::KirschProductsController < AdminController
  def index
    @kirsch_products = KirschProduct.all
  end

  def destroy
    KirschProduct.find(params[:id]).destroy!
    redirect_to admin_kirsch_products_path
  end
end