class Admin::OrdersController < AdminController
  def index
    true_orders
  end

  def mark_paid
    true_orders
    @order = Order.find(params[:order_id])
    @order.paid = true
    @order.save!
    redirect_to :admin_orders
  end

  def mark_sent
    true_orders
    @order = Order.find(params[:order_id])
    @order.sent = true
    @order.save!
    redirect_to :admin_orders
  end

  def mark_fake
    true_orders
    @order = Order.find(params[:order_id])
    @order.fake = true
    @order.save!
    redirect_to :admin_orders
  end

  def mark_ordered_at_kirsch
    true_orders
    @order = Order.find(params[:order_id])
    @order.orderd_at_kirsch = true
    @order.save!
    redirect_to :admin_orders
  end

  def fake
    @orders = Order.where(fake: true).all
  end

  private

  def true_orders
    @orders = Order.where(fake: false).all.sort_by(&:created_at).reverse
  end
end