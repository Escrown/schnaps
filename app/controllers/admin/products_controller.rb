class Admin::ProductsController < AdminController
  include ApplicationHelper

  # GET new_product
  def new
    @product = Product.new
  end

  # POST product
  def create
    params.permit!
    @product = Product.new(params[:product])
    if @product.save
      redirect_to admin_products_path
    else
      redirect_to new_admin_product_path
    end
  end

  # GET all products
  def index
    @products = Product.all
  end

  # DELETE product
  def destroy
    Product.find(params[:id]).destroy!
    redirect_to admin_products_path
  end

  def show
    @product = Product.find(params[:id])
  end
end