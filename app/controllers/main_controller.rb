class MainController < ApplicationController

  def landing_page
    # Neue Nutzer sollen gleich alle Produkte sehen. Lieber nur Angebote?
    @products = Product.all
  end

  def imprint
  end

  def agb
  end

  def scotch
  end

  def whiskey
  end

  def rum
  end

  def gin
  end

  def andere
  end

end