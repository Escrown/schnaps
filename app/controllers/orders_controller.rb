class OrdersController < ApplicationController
  # Step 1/3
  # prepare by selecting products
  def new
    @order = Order.new
    @order.order_items = OrderItem.with_versand
  end

  # Step 2/3
  # customer data entry
  def entry
    params.permit!
    @order = Order.new(params[:order])
    @order.order_items = @order.order_items.select  {|oi| oi.quantity == 1}
  end

  # Step 3/3
  # conduct order
  def create
    params.permit!
    @order = Order.new(params[:order])
    if @order.save
      OrderConfirmatorMailer.confirm(@order).deliver_now
      redirect_to @order
    else
      render html: "FEHLER! E-Mail richtig? Ansonsten anrufen #{TheCompany.phone}"
    end
  end

  def show # thank you page
    @order = Order.find(params[:id])
  end
end