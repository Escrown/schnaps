class ApplicationMailer < ActionMailer::Base
  default from: 'info@billiger-schnaps.de'
  layout 'mailer'
end
