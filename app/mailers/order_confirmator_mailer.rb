class OrderConfirmatorMailer < ApplicationMailer
  URL = 'billiger-schnaps.de'

  default from: "info@#{URL}",
          bcc: ["marketing@#{URL}", "info@#{URL}"]

  def confirm(order)
    @order = order
    mail(to: @order.email, subject: "#{URL} Bestellung ##{order.id}")
  end
end