class BankAccount < ApplicationRecord

  def self.owner
    'FrachtenFuchs UG'
  end

  def self.bic
    'HOLVDEB1'
  end

  def self.iban
    'DE92100179971590672794'
  end
end
