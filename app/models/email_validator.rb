class EmailValidator < ActiveModel::Validator
  def validate(record)
    emails = ['info@billiger-schhnaps.de', 'marketing@billiger-schnaps.de']
    email = record[:email]
    emails.include?(email)
  end
end