class KirschProduct < ApplicationRecord
  validates :fahrtenfuchs_price_in_cents, numericality: { greater_than_or_equal_to: :price_in_cents }
end