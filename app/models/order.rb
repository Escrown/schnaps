class Order < ApplicationRecord
  has_many :order_items

  validates :address, presence: true
  validates :email, presence: true, format: Devise.email_regexp

  accepts_nested_attributes_for :order_items, :allow_destroy => true

  before_save :save_prices

  def gross
    read_attribute(:gross) || order_items.sum(&:gross)
  end

  def net
    read_attribute(:net) || order_items.sum(&:net)
  end

  def vat
    read_attribute(:vat) || order_items.sum(&:vat)
  end

  def sent?
    !!read_attribute(:sent)
  end

  def pay!
    write_attribute(:paid, true)
  end

  def sent!
    write_attribute(:sent, true)
  end

  private

  def save_prices
    write_attribute(:net, order_items.sum(&:net))
    write_attribute(:vat, order_items.sum(&:vat))
    write_attribute(:gross, order_items.sum(&:gross))
  end
end
