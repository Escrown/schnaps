class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  accepts_nested_attributes_for :product

  validates :order, presence: true
  validates :product, presence: true

  before_save do
    net = product.net * quantity
    gross = product.gross * quantity

    write_attribute(:net, net.round(2))
    write_attribute(:gross, gross.round(2))
    write_attribute(:vat, (gross - net).round(2))
  end

  def title
    product.nil? ? 'HACK' : product.title
  end

  def net
    read_attribute(:net) || product.net * quantity
  end

  def vat
    read_attribute(:vat) || (product.gross - product.net) * quantity
  end

  def gross
    read_attribute(:gross) || product.gross  * quantity
  end

  def self.without_versand
    order_items_from_products(Product.without_versand)
  end

  def self.with_versand
    order_items_from_products(Product.all)
  end

  private

  def self.order_items_from_products(ps)
    ps.map do |p|
      OrderItem.new(
          product: p,
          net: p.net,
          gross: p.gross,
          quantity: 0
      )
    end
  end
end
