class Product < ApplicationRecord
  default_scope { order("created_at DESC") }
  has_one_attached :pic
  has_one_attached :vid

  validates :fahrtenfuchs_price_in_cents, numericality: { greater_than_or_equal_to: :price_in_cents }
  validates :title, presence: true
  validates :net_price_in_cents, presence: true, format: { with: /\A\d+(?:\.\d{0,2})?\z/ }, numericality: { greater_than: 0 }

  def self.without_versand
    self.all.where.not(title: 'Versand (national)')
  end

  def vat_rate
    after_start = Date.today >= Date.new(2020, 7, 1)
    before_end = Date.today <= Date.new(2020, 12, 31)
    if after_start && before_end
      0.16
    else
      0.19
    end
  end

  def vat_in_cents
    (vat_rate * net_price_in_cents).to_i
  end

  def vat
    vat_in_cents.to_f / 100.0
  end

  def net
    (net_price_in_cents.to_f/100.0).round(2)
  end

  def gross_price_in_cents
    net_price_in_cents + vat_in_cents
  end

  def gross
    (gross_price_in_cents / 100.0).to_f
  end
end