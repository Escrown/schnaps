class TheCompany

  def self.name_and_type
    "FrachtenFuchs UG (haftungsbeschränkt)"
  end

  def self.owner
    "Julian Hauck (Geschäftsführer)"
  end

  def self.street
    "Siegburger Straße"
  end

  def self.no
    "62"
  end

  def self.city
    "Bonn"
  end

  def self.plz
    "53229"
  end

  def self.phone
    "+49 - (0)170 41 63 26 4"
  end

  def self.email
    "info@billiger-schnaps.de"
  end

  def self.iban
    "DE92100179972590672794"
  end

  def self.bic
    "HOLVDEB1"
  end

  def self.website
    'billiger-schnaps.de'
  end

  def self.street_and_no
    "#{street} #{no}"
  end
end