require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module BilligerSchnaps
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.database_port = 5432

    unless Rails.env == :development || Rails.env == 'development'
      # Sentry
      config.filter_parameters << :password
      Raven.configure do |config|
        config.dsn = 'https://74c4ed82ed11470892f8e4f12a7b181d:321f5879402e48739050db6d16267ea6@o379868.ingest.sentry.io/5250131'
      end
    end

  end
end
