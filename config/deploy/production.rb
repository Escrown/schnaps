# config/deploy/production.rb

set :stage, :production

set :deploy_to, '/var/www/schnaps_p'
set :rails_env, 'production'
set :branch, 'master'

server 'app01-prod.chandra.makandra.de', user: 'deploy-schnaps_p', roles: %w(app web cron db) # first is primary
server 'app02-prod.chandra.makandra.de', user: 'deploy-schnaps_p', roles: %w(app web)

