# config/deploy/staging.rb

set :stage, :staging

set :deploy_to, '/var/www/schnaps_s/'
set :rails_env, 'staging'
set :branch, ENV['DEPLOY_BRANCH'] || 'master'

server 'app01-prod.chandra.makandra.de', user: 'deploy-schnaps_s', roles: %w(app web cron db) # first is primary
server 'app02-prod.chandra.makandra.de', user: 'deploy-schnaps_s', roles: %w(app web)

