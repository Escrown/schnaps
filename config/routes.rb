Rails.application.routes.draw do
  devise_for :users

  # Area 1: Admin
  namespace :admin do
    root to: 'home#index'
    match 'sign_in', :ur => "authentication#sign_in", via: :get
    match 'login', :to => "authentication#login", via: :post
    resources :products
    resources :kirsch_products
    resources :users
    resources :orders, only: [ :index ]
    namespace :orders do
      get :mark_paid, as: :mark_paid
      get :mark_sent, as: :mark_sent
      get :mark_fake, as: :mark_fake
      get :ordered_at_kirsch, as: :ordered_at_kirsch
      get :fake
    end
    get 'empty', to: 'home#empty'
    get 'reporting', to: 'home#reporting'
  end

  # Area 2: customer
  root to: 'main#landing_page'
  resources :orders, only: [:new, :create, :show]
  post 'orders/entry', to: 'orders#entry', as: 'entry'
  resources :products, only: [:show]

  get 'scotch', to: 'main#scotch', as: :scotch
  get 'whiskey', to: 'main#whiskey', as: :whiskey
  get 'rum', to: 'main#rum', as: :rum
  get 'gin', to: 'main#gin', as: :gin
  get 'andere', to: 'main#andere', as: :andere

  # Area 3: static pages
  get 'imprint', to: 'main#imprint'
  get 'agb', to: 'main#agb'
  get 'therapien', to: 'main#therapien'
end