RAILS_ROOT = File.expand_path(File.dirname(__FILE__) + '/')

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 1.month, :at => "9am" do
  products = []
  products << Product.new(title: 'Schnaps des Monats 1 - ', description: '', net_price_in_cents: 0) # id 0
  products << Product.new(title: 'Schnaps des Monats 2 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 3 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 4 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 5 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 6 - ', description: '', net_price_in_cents: 0) # id 5
  products << Product.new(title: 'Schnaps des Monats 7 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 8 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 9 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 10 - ', description: '', net_price_in_cents: 0)
  products << Product.new(title: 'Schnaps des Monats 11 - ', description: '', net_price_in_cents: 0) # id 10
  products << Product.new(title: 'Schnaps des Monats 12', description: '', net_price_in_cents: 0)


  year = Date.now.year
  month = Date.now.month
  #products[(year-2020+month-6)%12].save!
end

every 1.day, at: "9am" do
  #orders = Order.where(ordered_at_kirsch: false)
end