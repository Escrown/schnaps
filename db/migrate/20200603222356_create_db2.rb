class CreateDb2 < ActiveRecord::Migration[5.2]
  def change
    drop_table :order_items
    drop_table :orders
    drop_table :products

    create_table :orders do |t|
      t.string 'name'
      t.string 'address'
      t.string 'email'
      t.decimal 'gross'
      t.decimal 'net'
      t.decimal 'vat'
      t.timestamps
    end

    create_table :products do |t|
      t.string 'title', null: false
      t.string 'description'
      t.integer 'net_price_in_cents', null: false
      t.integer 'volume'
      t.integer 'size_in_ml'
      t.timestamps
    end

    create_table :order_items do |t|
      t.bigint "quantity", default: 0, null: false
      t.decimal "net", precision: 15, scale: 2, null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.bigint "order_id", null: false
      t.integer "product_id"
      t.decimal "vat"
      t.decimal "gross", precision: 15, scale: 2, null: false
      t.timestamps
    end
  end
end
