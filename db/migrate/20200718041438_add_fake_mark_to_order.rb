class AddFakeMarkToOrder < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :fake, :binary
  end
end
