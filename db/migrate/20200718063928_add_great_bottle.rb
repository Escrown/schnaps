class AddGreatBottle < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :great_bottle, :integer

    create_table :kirsch_products do |t|
      t.string :name
      t.string :article_number, unique: true
      t.integer :volume
      t.boolean :great
      t.integer :price_in_cents
      t.integer :frachtenfuchs_price_in_cents
      t.timestamps
    end
  end
end
