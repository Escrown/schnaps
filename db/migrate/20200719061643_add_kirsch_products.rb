class AddKirschProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :kirsch_products do |t|
      t.string :article_number, unique: true
      t.integer :price_in_cents
      t.integer :fahrtenfuchs_price_in_cents
      t.boolean :great
      t.timestamps
    end
  end
end
