class OrderedAtKirsch < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :ordered_at_kirsch, :boolean
  end
end
